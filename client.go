package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {

	// connect to this socket
	conn, _ := net.Dial("tcp", "127.0.0.1:8081")


fmt.Print("Text to send: ")

	for {

		// read in input from stdin
		reader := bufio.NewReader(os.Stdin)

		s, _ := reader.ReadString('\n')


		// send to socket
		fmt.Fprintf(conn, s)

		// listen for reply
		go func() {
		message, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Print("\n")
		fmt.Print("Message from server: " + message + "\n")
		fmt.Print("Text to send: ")
			}()

	}
}
