package main

import (
  "bufio"
  "fmt"
  "net"
  "strconv"
  "math"
  "os"
  "strings"
)

func numberFromString(s string) int {
	number := 0
	for i := 0; i <= len(s) - 1 ; i++ {
		f := string([]rune(s)[i])
		c, err := strconv.Atoi(f)
		if err == nil {
			//poate face convert in int
			//transform in numar cifrele
			number = number*10 + c
		}
	}
	
	if number == 0 {
		return -1
	}
	
	return int(number)
}

func handleConnection(conn net.Conn) {
    buf := bufio.NewReader(conn)

    var strRemoteAddr string
    strRemoteAddr = conn.RemoteAddr().String()
    fmt.Print("Client " + strRemoteAddr + " conected." + "\n")

    for {
        message, err := buf.ReadString('\n')

        if err != nil {
            fmt.Printf("Client disconnected.\n")
            break
        }

        var patratPerfTotal int
        patratPerfTotal = 0

        messageSlice := strings.Split(message, " ")

        for i := range messageSlice { //range returns both the index and value
          if messageSlice[i] != "\n" {

            var number int
            var patratPerf int
            patratPerf = 0
            number = numberFromString(messageSlice[i])

          if err == nil {

              // For Loop used in format of While Loop
              floatNumber := float64(number)

            	var y int = int(math.Sqrt(floatNumber))

            	if math.Sqrt(floatNumber) == float64(y) {
            		patratPerf = patratPerf + 1
            	}

              }
              patratPerfTotal = patratPerfTotal + patratPerf
          }
        }
        newString := strconv.Itoa(patratPerfTotal)

        // send new string back to client
        conn.Write([]byte(newString + "\n"))
        fmt.Print(strRemoteAddr + " a primit ca sunt : ")
        fmt.Print(patratPerfTotal)
        fmt.Print(" patrate perfecte" + "\n")

}



  }


func main() {

    server, err := net.Listen("tcp", ":8081")
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    for {
        fmt.Println("Waiting for client")
        conn, err := server.Accept()
        fmt.Println("Client connected")
        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }

        go handleConnection(conn)
    }
}
